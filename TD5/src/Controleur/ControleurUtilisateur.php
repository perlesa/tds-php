<?php
use App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        self::afficherVue('vueGenerale.php',["parametres" => ModeleUtilisateur::recupererUtilisateurs(),"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetail() : void {
        if($_GET['login']){
            self::afficherVue('vueGenerale.php',["parametres" => ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']),"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
        else{
            self::afficherVue('vueGenerale.php',["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php',["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void {
        $nouveauUtilisateur = ModeleUtilisateur::construireDepuisTableauSQL($_GET);
        $nouveauUtilisateur->ajouter();
        self::afficherVue('vueGenerale.php',["parametres" => ModeleUtilisateur::recupererUtilisateurs(),"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }
}
