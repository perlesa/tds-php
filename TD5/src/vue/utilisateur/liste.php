<!DOCTYPE html>
<html lang="fr">
<body>
<?php
/** @var ModeleUtilisateur[] $parametres */

use App\Covoiturage\Modele\ModeleUtilisateur;

foreach ($parametres as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    $url = 'http://localhost/tds-php/TD5/web/controleurFrontal.php?action=afficherDetail&login=' . $loginURL;

    echo '<p>Utilisateur de login ' . '<a href="' . htmlspecialchars($url) . '">' . $loginHTML . '</a>.</p>';
}
echo '<a href="http://localhost/tds-php/TD5/web/controleurFrontal.php?action=afficherFormulaireCreation">' . htmlspecialchars("Creer un nouvel utilisateur") . '</a>';
?>
</body>
</html>
