<!DOCTYPE html>
<html lang="fr">
<body>
<?php
use App\Covoiturage\Modele\ModeleUtilisateur;
/** @var ModeleUtilisateur $parametres */
$nomHTML = htmlspecialchars($parametres->getNom());
$prenomHTML = htmlspecialchars($parametres->getPrenom());
$loginHTML = htmlspecialchars($parametres->getLogin());
echo $nomHTML . " " . $prenomHTML . " " . $loginHTML;
?>
</body>
</html>