<?php
require_once "ConnexionBaseDeDonnees.php";

// On affiche un attribut de PDO pour vérifier  que la connexion est bien établie.
echo ConnexionBaseDeDonnees::getPdo()->getAttribute(PDO::ATTR_CONNECTION_STATUS);
?>

