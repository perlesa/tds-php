<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    public function __construct(
        string $login,
        string $nom,
        string $prenom
    ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom) : void {
        $this->nom = $nom;
    }

    public function getLogin() : string
    {
        return $this->login;
    }

    public function setLogin(string $login) : void
    {
        $this->login = substr($login,0,64);
    }

    public function getPrenom() : string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom) : void
    {
        $this->prenom = $prenom;
    }

    public function __toString() : string {
        return $this->nom . " " . $this->prenom . " " . $this->login;
    }

    // Méthode statique pour récupérer tous les utilisateurs
    public static function recupererUtilisateurs(): array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }
}
