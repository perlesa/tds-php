<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie {

    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        if ($dureeExpiration === null) {
            $dureeExpiration = 0;
        }
        $valeurSerializee = serialize($valeur);
        setcookie($cle, $valeurSerializee, time() + 7200 + $dureeExpiration);
    }

    public static function lire(string $cle): mixed
    {
        if (isset($_COOKIE[$cle])) {
            $valeurSerializee = $_COOKIE[$cle];
            $valeurDeserializee = unserialize($valeurSerializee);
            return $valeurDeserializee;
        }
        return null;
    }

    public static function contient($cle) : bool
    {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void
    {
        if (isset($_COOKIE[$cle])) {
            unset($_COOKIE[$cle]);
            setcookie($cle, "", 1);
        }
    }

}