<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../ressources/style.css">
    <title>
        <?php
        /**
         * @var string $titre
         */
        echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src= "../../ressources/heart.png"></a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherFormulaireCreation"><img src= "../../ressources/add-user.png"></a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage...
    </p>
</footer>
</body>
</html>