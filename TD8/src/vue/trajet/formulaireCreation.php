<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='controleur' value='trajet'>
    <input type='hidden' name='action' value='creerDepuisFormulaire'>
    <fieldset>
        <legend>Créer un nouveau trajet :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="id_id">ID</label>
            <input class="InputAddOn-field" type="text" placeholder="Sera généré automatiquement" name="id" id="id_id" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Départ&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Paris" name="depart" id="depart_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Lyon" name="arrivee" id="arrivee_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" name="date" id="date_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" step="0.01" placeholder="Ex : 25.50" name="prix" id="prix_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : jdupont" name="conducteurLogin" id="conducteurLogin_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non fumeur</label>
            <input class="InputAddOn-field" type="checkbox" name="nonFumeur" id="nonFumeur_id">
        </p>
        <p>
            <input type="submit" value="Créer le trajet" />
        </p>
    </fieldset>
</form>
