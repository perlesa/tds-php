<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('/vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
        if ($utilisateur === null) {
            self::afficherVue('vueGenerale.php', ["titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => "Utilisateur inconnu"]);
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($_GET['login']);
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", "utilisateur" => $utilisateur]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];
        $mdp = $_GET['mdp'];
        $mdp2 = $_GET['mdp2'];
        if($mdp != $mdp2){
            self::afficherErreur("Mots de passe distincts");
        }
        else{
            $utilisateur = new Utilisateur($login, $prenom, $nom, $mdp);
            (new UtilisateurRepository)->ajouter($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "login" => $login]);
        }
    }


    public static function mettreAJour(): void
    {
        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];
        $mdp = $_GET['mdp'];
        $utilisateur = new Utilisateur($login, $prenom, $nom, $mdp);
        (new UtilisateurRepository)->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", "login" => $login]);
    }

    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function supprimer(): void
    {
        $login = $_GET['login'];
        (new UtilisateurRepository)->supprimerParLogin($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", "login" => $login]);
    }

    /**
     * @return array
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $nom = $tableauDonneesFormulaire['nom'];
        $mdpHache = $tableauDonneesFormulaire['mdp'];
        return array($login, $prenom, $nom, $mdpHache);
    }

    public static function testerSession(): void
    {
        $session = Session::getInstance();
        $session->enregistrer("testNom", "testValeur");
        echo $session->lire("testNom");
    }

    /*
    public static function deposerCookie($cle, $valeur, $expiration): void
    {
        Cookie::enregistrer($cle, $valeur, $expiration);
    }

    public static function lireCookie($cle): ?string
    {
        return Cookie::lire($cle);
    }

    public static function supprimerCookie($cle): void
    {
        Cookie::supprimer($cle);
    }
    */
}
?>