<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class ControleurUtilisateur {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('/vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
        if ($utilisateur === null) {
            self::afficherVue('vueGenerale.php', ["titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => "Utilisateur inconnu"]);
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($_GET['login']);
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", "utilisateur" => $utilisateur]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];
        $utilisateur = new Utilisateur($login, $prenom, $nom);
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "login" => $login]);
    }


    public static function mettreAJour(): void
    {
        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];
        $utilisateur = new Utilisateur($login, $prenom, $nom);
        (new UtilisateurRepository)->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", "login" => $login]);
    }

    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function supprimer(): void
    {
        $login = $_GET['login'];
        (new UtilisateurRepository)->supprimerParLogin($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", "login" => $login]);
    }

    /**
     * @return array
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $nom = $tableauDonneesFormulaire['nom'];
        return array($login, $prenom, $nom);
    }
}
?>