<?php
use App\Covoiturage\Controleur\ControleurUtilisateur;
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// On récupère l'action passée dans l'URL
$action = isset($_GET['action']) ? $_GET['action'] : 'afficherListe';

// On récupère le contrôleur passé dans l'URL
$controleur = isset($_GET['controleur']) ? $_GET['controleur'] : 'utilisateur';

// Création du nom de la classe du contrôleur
$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);

// Vérification de l'existence de la classe du contrôleur
if (class_exists($nomDeClasseControleur)) {
    // Vérification de l'existence de la méthode (action)
    if (method_exists($nomDeClasseControleur, $action)) {
        // Appel de la méthode statique $action de la classe du contrôleur
        $nomDeClasseControleur::$action();
    } else {
        // Si l'action n'existe pas, on affiche une erreur
        $nomDeClasseControleur::afficherErreur();
    }
} else {
    // Si la classe du contrôleur n'existe pas, on affiche une erreur
    $nomDeClasseControleur::afficherErreur();
}
?>
