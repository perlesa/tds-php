<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        self::afficherVue('utilisateur/liste.php',ModeleUtilisateur::recupererUtilisateurs());
    }

    public static function afficherDetail() : void {
        if($_GET['login']){
            self::afficherVue('utilisateur/detail.php', [ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login'])]);
        }
        else{
            self::afficherVue('utilisateur/erreur.php');
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire() : void {
        $nouveauUtilisateur = ModeleUtilisateur::construireDepuisTableauSQL($_GET);
        $nouveauUtilisateur->ajouter();
        self::afficherVue('utilisateur/liste.php',ModeleUtilisateur::recupererUtilisateurs());
    }
}
?>