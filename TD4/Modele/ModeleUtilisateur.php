<?php
require_once "ConnexionBaseDeDonnees.php";
class ModeleUtilisateur {


    private string $login;
    private string $nom;
    private string $prenom;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager = null;


    public function __construct(
        string $login,
        string $nom,
        string $prenom
    ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur {
        return new ModeleUtilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom) : void {
        $this->nom = $nom;
    }

    public function getLogin() : string
    {
        return $this->login;
    }

    public function setLogin(string $login) : void
    {
        $this->login = substr($login,0,64);
    }

    public function getPrenom() : string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom) : void
    {
        $this->prenom = $prenom;
    }

   // public function __toString() : string {
    //    return $this->nom . " " . $this->prenom . " " . $this->login;
   // }

    public function getTrajetsCommePassager(): array {
        if ($this->trajetsCommePassager === null) {
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajets): void {
        $this->trajetsCommePassager = $trajets;
    }


    // Méthode statique pour récupérer tous les utilisateurs
    public static function recupererUtilisateurs(): array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if(!$utilisateurFormatTableau) {
            return null;
        }

        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter(): bool {
        $sql = "INSERT INTO utilisateur(login, nom, prenom) VALUES(:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = [
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom,
        ];

        try {
            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }


    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager(): array {
        $sql = "SELECT t.* FROM trajet t
            JOIN passager p ON t.id = p.trajetId
            WHERE p.passagerLogin = :loginTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = [
            "loginTag" => $this->login,
        ];
        $pdoStatement->execute($values);

        $trajets = [];
        while ($row = $pdoStatement->fetch()) {
            $trajets[] = Trajet::construireDepuisTableauSQL($row); // Méthode de la classe Trajet
        }

        return $trajets;
    }

    public function supprimerPassager(string $passagerLogin): bool {
        $sql = "DELETE FROM passager WHERE trajetId = :trajetId AND passagerLogin = :loginTag";
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->prepare($sql);

        $values = [
            "trajetId" => $this->id,
            "loginTag" => $passagerLogin,
        ];

        try {
            $pdoStatement->execute($values);
            if ($pdoStatement->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log("Erreur lors de la suppression du passager : " . $e->getMessage());
            return false;
        }
    }




}

