<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Liste des utilisateurs :
        <?php
        $utilisateur1 = array(
                'nom' => 'perles',
                'prenom' => 'alexis',
                'login' => 'perlesa'
        );

        $utilisateur2 = array(
            'nom' => 'marzewski',
            'prenom' => 'antoni',
            'login' => 'marzewskia'
        );

        $utilisateur3 = array(
            'nom' => 'eduardo',
            'prenom' => 'mayitone',
            'login' => 'mayitonee'
        );

        $utilisateurs = array($utilisateur1, $utilisateur2, $utilisateur3);

        if (empty($utilisateurs)) {
            echo "Il n’y a aucun utilisateur.";
        } else {
        echo "<ul>";
        foreach ($utilisateurs as $utilisateur) {
            echo "<li>Nom : " . $utilisateur['nom'] .
                " Prénom : " . $utilisateur['prenom'] .
                " Login : " . $utilisateur['login'] . "</li>";
        }
        echo "</ul>";}
        ?>
    </body>
</html>