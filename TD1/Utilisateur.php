<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) : void {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login) : void
    {
        $this->login = substr($login,0,64);
    }

    /**
     * @return mixed
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom) : void
    {
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string {
        return $this->nom . " " . $this->prenom . " " . $this->login;
    }

}
