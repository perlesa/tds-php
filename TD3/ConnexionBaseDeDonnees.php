<?php
require_once 'ConfigurationBaseDeDonnees.php';
class ConnexionBaseDeDonnees {
    private static ?ConnexionBaseDeDonnees $instance = null;

    private PDO $pdo;

    public function __construct()
    {
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $login = ConfigurationBaseDeDonnees::getLogin();
        $port = ConfigurationBaseDeDonnees::getPort();
        $motDePasse = ConfigurationBaseDeDonnees::getPassword();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        // Connexion à la base de données
// Le dernier argument sert à ce que toutes les chaines de caractères
// en entrée et sortie de MySql soient dans le codage UTF-8
        $this->pdo = new PDO("mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees", $login, $motDePasse,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }


    public static function getPdo(): PDO {
        return ConnexionBaseDeDonnees::getInstance()->pdo;
    }

    private static function getInstance() : ConnexionBaseDeDonnees {
        // L'attribut statique $instance s'obtient avec la syntaxe ConnexionBaseDeDonnees::$instance
        if (is_null(ConnexionBaseDeDonnees::$instance))
            // Appel du constructeur
            ConnexionBaseDeDonnees::$instance = new ConnexionBaseDeDonnees();
        return ConnexionBaseDeDonnees::$instance;
    }


}