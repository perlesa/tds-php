<?php
require_once "ConnexionBaseDeDonnees.php";
require_once "Utilisateur.php";

$utilisateurs = Utilisateur::recupererUtilisateurs();

foreach($utilisateurs as $utilisateur) {
    $trajets = $utilisateur->getTrajetsCommePassager();
    foreach($trajets as $trajet) {
        echo $trajet->__toString() . '<br>';
    }
}
