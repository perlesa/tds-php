<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use Exception;


class UtilisateurRepository extends AbstractRepository
{
    public function getNomTable(): string
    {
        return "utilisateur";

    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }



    public function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $objetFormatTableau["login"],
            $objetFormatTableau["prenom"],
            $objetFormatTableau["nom"]
        );
    }

    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        $utilisateur = $objet;
        return array(
            "login" => $utilisateur->getLogin(),
            "nom" => $utilisateur->getNom(),
            "prenom" => $utilisateur->getPrenom(),
        );
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager() : array {
        $sql = "SELECT T.* FROM trajet T 
            JOIN passager P ON P.trajetId = T.id 
            WHERE P.passagerLogin = :loginTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $this->login,
        );

        $pdoStatement->execute($values);

        $trajets = [];
        while ($row = $pdoStatement->fetch()) {
            $trajets[] = (new TrajetRepository)->construireDepuisTableauSQL($row);
        }
        return $trajets;
    }
}

